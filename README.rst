Attempt to create completefunc which would look for all files in 
the current directory.

Requires external grep command, so Windows users have to install 
some port of it. (requires ``-h`` and ``-o`` non-POSIX switches, 
but all BSD and GNU versions of grep have them).

Originally answer_ from StackExchange.

.. _answer:
   https://stackoverflow.com/a/18203966/164233

For activation add::

   set completefunc=filescompl#complete

to your ``~/.vimrc`` and then you can use ``<C-X><C-U>`` to 
invoke the completion.
